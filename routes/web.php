<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () { return redirect(route("inicio")); });
    Route::get('/home', function () { return redirect(route("inicio")); });

    Route::get('/inicio', 'principalController@index')->name('inicio');

    Route::get('/maquinas', 'MaquinasController@index')->name('maquinas');
    Route::get('/maquinas/tabla', 'MaquinasController@tabla')->name('maquinas_tabla');

    Route::get('/maquinas/crear', 'MaquinasController@crear')->name('maquinas_crear');
    Route::post('/maquinas/crear/save', 'MaquinasController@guardar')->name('maquinas_save');

    Route::get('/maquinas/{id}/ver', 'MaquinasController@ver')->name('maquinas_ver');
    Route::get('/maquinas/{id}/mantenimientos', 'MantenimientosController@mantenimientosMaquina')->name('maquinas_mantenimientos');
    Route::get('/maquinas/{id}/editar', 'MaquinasController@editar')->name('maquinas_editar');
    Route::post('/maquinas/{id}/editar/update', 'MaquinasController@actualizar')->name('maquinas_update');
    Route::get('/maquinas/eliminar', 'MaquinasController@eliminar')->name('maquinas_eliminar');

    Route::get('/mantenimientos', 'MantenimientosController@index')->name('mantenimientos');
    Route::get('/mantenimientos/tabla', 'MantenimientosController@tabla')->name('mantenimientos_tabla');
    Route::get('/mantenimientos/crear', 'MantenimientosController@crear')->name('mantenimientos_crear');
    Route::post('/mantenimientos/crear/save', 'MantenimientosController@guardar')->name('mantenimientos_save');
    Route::get('/mantenimientos/{id}/editar', 'MantenimientosController@editar')->name('mantenimientos_editar');
    Route::post('/mantenimientos/{id}/editar/update', 'MantenimientosController@actualizar')->name('mantenimientos_update');

    Route::get('/alarmas/tablaProximaFecha', 'AlarmasController@tablaProximaFecha')->name('alarmas_proxima_fecha');
    Route::get('/alarmas/tablaAveriadas', 'AlarmasController@tablaAveriadas')->name('alarmas_averiadas');
});


