<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = "historico_mantenimientos";

    public function maquina()
    {
        return  $this->hasOne(Maquina::class,"id","maquina_id");
    }
}
