<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maquina;
use App\Traits\NotificacionesTrait;
use DataTables;
use Carbon\Carbon;

class MaquinasController extends Controller
{

    /**
     * Metodo constructor
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function index()
    {
        return view('maquinas.inicio');
    }

    /**
     * Función encargada de retornar la configuración para
     * la información generada desde la tabla
     *
     */
    public function tabla()
    {
        return DataTables::of(Maquina::query())
        ->addColumn('maquina',function($maquina){
            return $maquina->codigo ." | ". $maquina->nombre;
        })
        ->addColumn('f_ultimo_mantenimiento',function($maquina){
            if (!$maquina->f_ultimo_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_ultimo_mantenimiento)->format('d/m/Y');
        })
        ->addColumn('f_proximo_mantenimiento',function($maquina){
            if (!$maquina->f_proximo_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_proximo_mantenimiento)->format('d/m/Y');
        })
        ->editColumn('estado',function($maquina){
            return $this->generadorTextoEstado($maquina->estado);
        })
        ->addColumn('acciones',function($maquina){
            $cant = $maquina->mantenimientos->count();
            $botones  = '<div class="button-group">';
            $botones .= '   <a class="btn btn-outline-primary" title="Ver maquina" href="'.route('maquinas_ver',$maquina->id).'"><i class="mdi mdi-eye"></i><a>';
            $botones .= '   <a class="btn btn-outline-info" title="Editar maquina" href="'.route('maquinas_editar',$maquina->id).'"><i class="mdi mdi-pencil"></i><a>';

            $clase_btn_eliminar = "btn_eliminar_maquina";
            $maquina_id = $maquina->id;
            if ($cant > 0) {
                $clase_btn_eliminar = "btn_eliminar_maquina_nop";
                $maquina_id = "empty";
            }

            $botones .= '   <button class="btn btn-outline-danger '.$clase_btn_eliminar.'" data-id="'.$maquina_id.'" title="Eliminar maquina"><i class="mdi mdi-close"></i></button>';

            $botones .= '</div>';

            return $botones;
        })
        ->rawColumns(['acciones'])
        ->make(true);
    }

    /**
     * Función encargada de retornar la vista que permite crear
     * nuevas maquinas
     *
     * @return view
     */
    public function crear(Request $request)
    {
        return view('maquinas.crear');
    }

    /**
     * Función encargada de almacenar la información de la nueva
     * maquina
     *
     * @return view
     */
    public function guardar(Request $request)
    {
        $codigo = $request->codigo;
        $nombre = $request->nombre;

        $maquina = new Maquina();
        $maquina->codigo = $codigo;
        $maquina->nombre = $nombre;
        $maquina->ubicacion = $request->ubicacion;
        $maquina->f_instalacion = Carbon::parse($request->f_instalacion." 00:00:00");
        $maquina->estado = $request->estado;
        $maquina->save();

        $this->notificarOk("La maquina \"$codigo | $nombre\" se creo de manera correcta.");
        return redirect(route('maquinas'));
    }

    /**
     * Función encargada de retornar la vista que muestra la información
     * de la maquina seleccionada
     *
     * @return view
     */
    public function ver(Request $request, $id)
    {
        $maquina = Maquina::find($id);
        if (!$maquina) {
            $this->notificarError("La maquina que intenta buscar no existe.");
            return redirect(route('maquinas'));
        }

        $maquina->estado = $this->generadorTextoEstado($maquina->estado);
        $maquina->f_proximo_mantenimiento = Carbon::parse($maquina->f_proximo_mantenimiento)->format('Y-m-d H:i');
        $maquina->f_ultimo_mantenimiento = Carbon::parse($maquina->f_ultimo_mantenimiento)->format('Y-m-d H:i');
        $maquina->f_instalacion = Carbon::parse($maquina->f_instalacion)->format('Y-m-d H:i');

        return view('maquinas.ver',compact('maquina'));
    }

    /**
     * Función encargada de retornar la vista permite modificar / actualizar
     * la maquina seleccionada
     *
     * @return view
     */
    public function editar(Request $request, $id)
    {
        $maquina = Maquina::find($id);
        if (!$maquina) {
            $this->notificarError("La maquina que intenta buscar no existe.");
            return redirect(route('maquinas'));
        }

        $maquina->f_instalacion = Carbon::parse($maquina->f_instalacion)->format('Y-m-d');

        return view('maquinas.editar',compact('maquina'));
    }

    /**
     * Función encargada de retornar la vista permite modificar / actualizar
     * la maquina seleccionada
     *
     * @return view
     */
    public function actualizar(Request $request, $id)
    {
        $maquina = Maquina::find($id);
        if (!$maquina) {
            $this->notificarError("La maquina que intenta buscar no existe.");
            return redirect(route('maquinas'));
        }

        $codigo = $request->codigo;
        $nombre = $request->nombre;

        $maquina->codigo = $codigo;
        $maquina->nombre = $nombre;
        $maquina->ubicacion = $request->ubicacion;
        $maquina->f_instalacion = Carbon::parse($request->f_instalacion." 00:00:00");
        $maquina->estado = $request->estado;
        $maquina->save();

        $this->notificarOk("La maquina \"$codigo | $nombre\" se actualizo de manera correcta.");
        return redirect(route('maquinas'));
    }

    /**
     * Función encargada de eliminar la maquina seleccionada
     *
     * @return view
     */
    public function eliminar(Request $request)
    {
        $maquina = Maquina::find($request->maquina_id);
        if ($maquina->mantenimientos->count() > 0) {
            return response("mal",500);
        }

        $maquina->delete();
        return response("ok",200);
    }

    /**
     * Función encargada de retornar el estado de la maquina interpretado en texto
     * Los valores retornados pueden ser los siguientes:
     * 1. Activo
     * 2. Inactivo
     * 3. Averiado
     */
    private function generadorTextoEstado($estado)
    {
        switch ($estado) {
            case 1:
                return "Activo";
                break;

            case 2:
                return "Inactivo";
                break;

            case 3:
                return "Averiado";
                break;

            default:
                return "N/A";
                break;
        }
    }
}
