<?php

namespace App\Http\Controllers;

use DataTables;
use App\Historico;
use App\Maquina;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MantenimientosController extends Controller
{
    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function index()
    {
        return view('mantenimientos.inicio');
    }

    public function tabla(Request $request)
    {
        return DataTables::of(Historico::with('maquina'))
        ->addColumn('tecnico',function($mantenimiento){
            return $mantenimiento->tecnico_tarjeta." | ".$mantenimiento->tecnico_nombre;
        })
        ->addColumn('maquina',function($mantenimiento){
            $maquina = $mantenimiento->maquina;
            return $maquina->codigo." | ".$maquina->nombre;
        })
        ->addColumn('f_mantenimiento',function($maquina){
            if (!$maquina->f_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_mantenimiento)->format('d/m/Y');
        })
        ->addColumn('acciones',function($maquina){
            $botones  = '<div class="button-group">';
            $botones .= '   <a class="btn btn-outline-info" title="Editar maquina" href="'.route('mantenimientos_editar',$maquina->id).'"><i class="mdi mdi-pencil"></i><a>';
            $botones .= '   <button class="btn btn-outline-danger btn_eliminar_maquina" data-id="'.$maquina->id.'" title="Eliminar maquina"><i class="mdi mdi-close"></i></button>';
            $botones .= '</div>';

            return $botones;
        })
        ->rawColumns(['acciones'])
        ->make(true);
    }

    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function crear()
    {
        $maquinas = Maquina::get();
        return view('mantenimientos.crear',compact('maquinas'));
    }

    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function guardar(Request $request)
    {
        $maquina = Maquina::find($request->maquina_id);
        if (!$maquina) {
            $this->notificarError("La maquina que intenta buscar no existe.");
            return redirect(route('mantenimientos'));
        }

        $mantenimiento = new Historico();
        $mantenimiento->maquina_id = $maquina->id;
        $mantenimiento->tecnico_tarjeta = $request->tecnico_tarjeta;
        $mantenimiento->tecnico_nombre = $request->tecnico_nombre;
        $mantenimiento->observaciones = $request->observaciones;
        $mantenimiento->f_mantenimiento = Carbon::parse($request->f_mantenimiento." 00:00:00");
        $mantenimiento->save();

        $maquina->f_ultimo_mantenimiento = $mantenimiento->f_mantenimiento;
        $maquina->f_proximo_mantenimiento = Carbon::parse($request->f_proximo_mantenimiento." 00:00:00");
        $maquina->save();

        $codigo = $maquina->codigo;
        $nombre = $maquina->nombre;

        $this->notificarOk("El mantenimiento para la maquina \"$codigo | $nombre\" se creo de manera correcta.");
        return redirect(route('mantenimientos'));
    }

    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function editar(Request $request, $id)
    {
        $mantenimiento = Historico::with('maquina')->find($id);
        if (!$mantenimiento) {
            $this->notificarError("El mantenimiento que intenta buscar no existe.");
            return redirect(route('mantenimientos'));
        }

        $mantenimiento->f_mantenimiento = Carbon::parse($mantenimiento->f_mantenimiento)->format('Y-m-d');
        $mantenimiento->f_mantenimiento = Carbon::parse($mantenimiento->f_mantenimiento)->format('Y-m-d');
        $mantenimiento->maquina->f_proximo_mantenimiento = Carbon::parse($mantenimiento->maquina->f_proximo_mantenimiento)->format('Y-m-d');

        $maquinas = Maquina::get();
        return view('mantenimientos.editar',compact('mantenimiento','maquinas'));
    }

    /**
     * Función encargada de retornar la vista principal del controlador,
     * desde esta vista se consumira la función "tabla"
     *
     * @return view
     */
    public function actualizar(Request $request, $id)
    {
        $mantenimiento = Historico::find($id);
        if (!$mantenimiento) {
            $this->notificarError("El mantenimiento consultado no existe.");
            return redirect(route('mantenimientos'));
        }

        $maquina = Maquina::find($request->maquina_id);
        if (!$maquina) {
            $this->notificarError("La maquina que intenta buscar no existe.");
            return redirect(route('mantenimientos'));
        }

        $mantenimiento->maquina_id = $maquina->id;
        $mantenimiento->tecnico_tarjeta = $request->tecnico_tarjeta;
        $mantenimiento->tecnico_nombre = $request->tecnico_nombre;
        $mantenimiento->observaciones = $request->observaciones;
        $mantenimiento->f_mantenimiento = Carbon::parse($request->f_mantenimiento." 00:00:00");
        $mantenimiento->save();

        $maquina->f_ultimo_mantenimiento = $mantenimiento->f_mantenimiento;
        $maquina->f_proximo_mantenimiento = Carbon::parse($request->f_proximo_mantenimiento." 00:00:00");
        $maquina->save();

        $codigo = $maquina->codigo;
        $nombre = $maquina->nombre;

        $this->notificarOk("El mantenimiento para la maquina \"$codigo | $nombre\" se actualizo de manera correcta.");
        return redirect(route('mantenimientos'));
    }

    public function mantenimientosMaquina(Request $request, $id)
    {
        return DataTables::of(Historico::where('maquina_id',$id))
        ->addColumn('tecnico',function($maquina){
            return $maquina->tecnico_tarjeta." | ".$maquina->tecnico_nombre;
        })
        ->addColumn('f_mantenimiento',function($maquina){
            if (!$maquina->f_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_mantenimiento)->format('d/m/Y');
        })
        ->make(true);
    }
}
