<?php

namespace App\Http\Controllers;

use App\Maquina;
use Illuminate\Http\Request;

class principalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $contProxMant = $this->contadorProximosMantenimientos();
        $contAveriadas = $this->contadorAveriadas();

        return view('inicio',compact('contAveriadas','contProxMant'));
    }

    /**
     * Función encargada de retornar la cantidad de maquinas averiadas
     *
     * @return Integer
     */
    public function contadorProximosMantenimientos()
    {
        return Maquina::whereIn('f_proximo_mantenimiento',[date('Y-m-d H:i:s'), date( "Y-m-d", strtotime( date('Y-m-d')." +7 day" ))])->count();
    }

    /**
     * Función encargada de retornar la cantidad de maquinas averiadas
     *
     * @return Integer
     */
    public function contadorAveriadas()
    {
        return Maquina::with('mantenimientos')->where('estado',3)->count();
    }
}
