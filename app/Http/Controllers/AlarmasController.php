<?php

namespace App\Http\Controllers;

use App\Maquina;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AlarmasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Función encargada de retornar la tabla de alertas por proxima
     * fecha de mantenimiento.
     *
     * @return DataTables
     */
    public function tablaProximaFecha(Request $request)
    {
        return DataTables::of(Maquina::whereIn('f_proximo_mantenimiento',[date('Y-m-d H:i:s'), date( "Y-m-d", strtotime( date('Y-m-d')." +7 day" ))]))
        ->addColumn('maquina',function($maquina){
            return $maquina->codigo." | ".$maquina->nombre;
        })
        ->editColumn('f_ultimo_mantenimiento',function($maquina){
            if (!$maquina->f_ultimo_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_ultimo_mantenimiento)->format('d/m/Y');
        })
        ->editColumn('f_proximo_mantenimiento',function($maquina){
            if (!$maquina->f_proximo_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_proximo_mantenimiento)->format('d/m/Y');
        })
        ->addColumn('acciones',function($maquina){
            $botones  = '<div class="button-group">';
            $botones .= '   <a class="btn btn-outline-primary" title="Ver maquina" href="'.route('maquinas_ver',$maquina->id).'"><i class="mdi mdi-eye"></i><a>';
            $botones .= '</div>';

            return $botones;
        })
        ->rawColumns(['acciones'])
        ->make(true);
    }

    /**
     * Función encargada de retornar la tabla de alertas por maquina con
     * averias.
     *
     * @return DataTables
     */
    public function tablaAveriadas(Request $request)
    {
        return DataTables::of(Maquina::with('mantenimientos')->where('estado',3))
        ->addColumn('maquina',function($maquina){
            return $maquina->codigo." | ".$maquina->nombre;
        })
        ->editColumn('f_ultimo_mantenimiento',function($maquina){
            if (!$maquina->f_ultimo_mantenimiento) {
                return "N/A";
            }

            return Carbon::parse($maquina->f_ultimo_mantenimiento)->format('d/m/Y');
        })
        ->addColumn('observaciones',function($maquina){
            $mantenimiento = $maquina->mantenimientos->first();
            if (!$mantenimiento) {
                return "N/A";
            }

            return $mantenimiento->observaciones;
        })
        ->make(true);
    }
}
