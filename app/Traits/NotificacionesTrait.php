<?php

namespace App\Traits;

use Alert;

trait NotificacionesTrait
{
    protected $titulo_error;
    protected $titulo_ok;

    public function inicializarTodo() {
        $this->titulo_error = env("ERROR_DEF_TITLE","Mal!");
        $this->titulo_ok = env("OK_DEF_TITLE","Bien!");
    }

    public function setTituloOk($titulo)
    {
        $this->titulo_ok = $titulo;
    }

    public function setTituloError($titulo)
    {
        $this->titulo_error = $titulo;
    }

    public function notificarError($contenido)
    {
        $this->inicializarTodo();
        alert()->error($this->titulo_error,$contenido);
    }

    public function notificarOk($contenido)
    {
        $this->inicializarTodo();
        alert()->success($this->titulo_ok,$contenido);
    }
}
