<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maquina extends Model
{
    protected $table = "maquinas";

    public function mantenimientos()
    {
        return $this->hasMany(Historico::class,"maquina_id","id")->orderBy('historico_mantenimientos.f_mantenimiento','desc');
    }
}
