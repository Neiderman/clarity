@extends('layouts.app')

@section('titulo')
Maquinas
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
        <div class="d-flex align-items-end flex-wrap"></div>
        <div class="d-flex justify-content-between align-items-end flex-wrap">
            <button onclick="location.href='{!! route('inicio') !!}';" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
                <i class="mdi mdi-home text-muted"></i>
            </button>
            <button onclick="location.href='{!! route('mantenimientos_crear') !!}';"  class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0" title="Crear nueva maquina">
                <i class="mdi mdi-plus text-muted"></i>
            </button>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">Listado de mantenimientos</h4>
            <div class="table-responsive">
                <table id="tabla_maquinas" class="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Tecnico
                            </th>
                            <th>
                                Maquina
                            </th>
                            <th>
                                Fecha
                            </th>
                            <th>
                                Observaciones
                            </th>
                            <th>
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
  @endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tabla_maquinas').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{!! route('mantenimientos_tabla') !!}",
            "language": {
                url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "columns": [
                { "data": "tecnico" },
                { "data": "maquina" },
                { "data": "f_mantenimiento" },
                { "data": "observaciones" },
                { "data": "acciones" }
            ]
        });

        $('body').delegate('.btn_eliminar_maquina','click', function(){
            alert('aa');
        });
    });
</script>
@endsection
