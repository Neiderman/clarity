@extends('layouts.app')

@section('titulo')
Mantenimientos
@endsection

@section('contenido')
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body">
        <h4 class="card-title">Crear mantenimiento</h4>
        <form method="POST" action="{{ route('mantenimientos_save') }}">
            @csrf
            <div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Maquinas</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="maquina_id" name="maquina_id" required>
                                    <option value="" {!! old('maquina_id') ? "" : "selected" !!}>Seleccione una maquina</option>
                                    @foreach ($maquinas as $maquina)
                                        <option value="{!! $maquina->id !!}" {!! old('maquina_id') == $maquina->id ? "selected" : ""!!}>{!! $maquina->codigo ." | ". $maquina->nombre !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">F. mantenimiento </label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" placeholder="dd/mm/yyyy" id="f_mantenimiento" name="f_mantenimiento" value="{!! old('f_mantenimiento') ?: date('Y-m-d') !!}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Tarjeta de tecnico</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="tecnico_tarjeta" name="tecnico_tarjeta" value="{!! old('tecnico_tarjeta') !!}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nombre de tecnico</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="tecnico_nombre" name="tecnico_nombre" value="{!! old('tecnico_nombre') !!}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">F. proximo mantenimiento</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" placeholder="dd/mm/yyyy" id="f_proximo_mantenimiento" name="f_proximo_mantenimiento" value="{!! old('f_proximo_mantenimiento') ?: date( "Y-m-d", strtotime( date('Y-m-d')." +7 day" )) !!}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label">Observación</label>
                            <div class="col-sm-11">
                                <textarea class="form-control" name="observaciones" id="observaciones" required>{!! old('observaciones') !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button-group">
                    <a href="{!! route('mantenimientos') !!}" class="btn btn-primary btn-lg" title="Volver al listado de mantenimientos">
                        <i class="mdi mdi-keyboard-return"></i>
                    </a>
                    <button type="submit" class="btn btn-info btn-lg">
                        <i class="mdi mdi-content-save"></i>
                        Crear
                    </button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
