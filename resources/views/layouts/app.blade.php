
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Claritydiagnostics | @yield('titulo')</title>
        <link rel="stylesheet" href="{!! asset("assets/recursos/mdi/css/materialdesignicons.min.css") !!}">
        <link rel="stylesheet" href="{!! asset("assets/recursos/base/vendor.bundle.base.css") !!}">
        <link rel="stylesheet" href="{!! asset("assets/recursos/datatables.net-bs4/dataTables.bootstrap4.css") !!}">
        <link rel="stylesheet" href="{!! asset("assets/css/style.css") !!}">
        <link rel="shortcut icon" href="{!! asset("assets/images/favicon.png") !!}" />
    </head>
    <body>
        <div class="container-scroller">

            <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="navbar-brand-wrapper d-flex justify-content-center">
                    <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
                    <a class="navbar-brand brand-logo" href="index.html"></a>
                    <a class="navbar-brand brand-logo-mini" href="index.html"></a>
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="mdi mdi-sort-variant"></span>
                    </button>
                    </div>
                </div>

                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                    <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                        <span class="nav-profile-name">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">

                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-logout text-primary"></i>
                            Cerrar sesión
                        </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                        </div>
                    </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>

            {{-- Inicio - Menu --}}
            <div class="container-fluid page-body-wrapper">
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('inicio') }}">
                            <i class="mdi mdi-home menu-icon"></i>
                            <span class="menu-title">Inicio</span>
                            </a>
                        </li>

                        {{-- <li class="nav-item">
                            <a class="nav-link collapsed" data-toggle="collapse" href="#alarmas_main" aria-expanded="false" aria-controls="alarmas_main">
                                <i class="mdi mdi-bell menu-icon"></i>
                                <span class="menu-title">Alarmas</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="alarmas_main" style="">
                                <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link" href="{!! route('alarmas') !!}">Listado</a></li>
                                </ul>
                            </div>
                        </li> --}}

                        <li class="nav-item">
                            <a class="nav-link collapsed" data-toggle="collapse" href="#maquinas_main" aria-expanded="false" aria-controls="maquinas_main">
                                <i class="mdi mdi-desktop-mac menu-icon"></i>
                                <span class="menu-title">Maquinas</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="maquinas_main" style="">
                                <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link" href="{!! route('maquinas') !!}">Listado</a></li>
                                <li class="nav-item"> <a class="nav-link" href="{!! route('maquinas_crear') !!}">Crear</a></li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link collapsed" data-toggle="collapse" href="#mantenimientos_main" aria-expanded="false" aria-controls="mantenimientos_main">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Mantenimientos</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="mantenimientos_main" style="">
                                <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link" href="{!! route('mantenimientos') !!}">Listado</a></li>
                                <li class="nav-item"> <a class="nav-link" href="{!! route('mantenimientos_crear') !!}">Crear</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </nav>
                {{-- Fin - Menu --}}

                <div class="main-panel">
                    <div class="content-wrapper">
                        {{-- Inicio - contenido --}}
                        @yield('contenido')
                        {{-- Fin - contenido --}}
                    </div>

                    <footer class="footer">
                        <div class="d-sm-flex justify-content-center justify-content-sm-between">
                            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © Esneider Mejia Ciro | {!! date('Y') !!}</span>
                            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard template</a> from Bootstrapdash.com</span>
                        </div>
                    </footer>
                </div>
            </div>
        </div>

        @include('sweetalert::alert')

        <script src="{!! asset("assets/recursos/base/vendor.bundle.base.js") !!}"></script>
        <script src="{!! asset("assets/recursos/chart.js/Chart.min.js") !!}"></script>
        <script src="{!! asset("assets/recursos/datatables.net/jquery.dataTables.js") !!}"></script>
        <script src="{!! asset("assets/recursos/datatables.net-bs4/dataTables.bootstrap4.js") !!}"></script>
        <script src="{!! asset("assets/js/jquery.dataTables.js") !!}"></script>
        <script src="{!! asset("assets/js/dataTables.bootstrap4.js") !!}"></script>
        <script src="{!! asset("assets/js/jquery.cookie.js") !!}" type="text/javascript"></script>

        @yield('scripts')
    </body>

</html>

