@extends('layouts.app')

@section('titulo')
Inicio
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
        <div class="card-body dashboard-tabs p-0">
            <ul class="nav nav-tabs px-4" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="list_prox_mant" data-toggle="tab" href="#list_prox_mant_" role="tab" aria-controls="list_prox_mant_" aria-selected="true">
                    Listado de proximos mantenimientos <button type="button" class="btn btn-outline-danger btn-icon-text">{!! $contProxMant !!}</button>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="list_averiadas" data-toggle="tab" href="#list_averiadas_" role="tab" aria-controls="list_averiadas_" aria-selected="false">
                    Listado de maquinas averiadas <button type="button" class="btn btn-outline-danger btn-icon-text">{!! $contAveriadas !!}</button>
                </a>
            </li>
            </ul>
            <div class="tab-content py-0 px-0">
                <div class="tab-pane fade active show" id="list_prox_mant_" role="tabpanel" aria-labelledby="list_prox_mant">
                    <div class="d-flex flex-wrap justify-content-xl-between">
                        <div class="d-none d-xl-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                            <div class="table-responsive">
                                <table id="tabla_alertas_mantenimientos" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Maquina
                                            </th>
                                            <th>
                                                Fecha ultimo mantenimiento
                                            </th>
                                            <th>
                                                Fecha proximo mantenimiento
                                            </th>
                                            <th>
                                                Acciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list_averiadas_" role="tabpanel" aria-labelledby="list_averiadas">
                    <div class="d-flex flex-wrap justify-content-xl-between">
                        <div class="d-none d-xl-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                            <div class="table-responsive">
                                <table id="tabla_alertas_averiados" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                Maquina
                                            </th>
                                            <th>
                                                Fecha ultimo mantenimiento
                                            </th>
                                            <th>
                                                Fecha proximo mantenimiento
                                            </th>
                                            <th>
                                                Observaciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tabla_alertas_mantenimientos').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{!! route('alarmas_proxima_fecha') !!}",
            "language": {
                url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "columns": [
                { "data": "maquina" },
                { "data": "f_ultimo_mantenimiento" },
                { "data": "f_proximo_mantenimiento" },
                { "data": "acciones" }
            ]
        });

        $('#tabla_alertas_averiados').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{!! route('alarmas_averiadas') !!}",
            "language": {
                url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "columns": [
                { "data": "maquina" },
                { "data": "f_proximo_mantenimiento" },
                { "data": "f_ultimo_mantenimiento" },
                { "data": "observaciones" }
            ]
        });
    });
</script>
@endsection
