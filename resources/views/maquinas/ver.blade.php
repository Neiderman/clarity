@extends('layouts.app')

@section('titulo')
Maquinas
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Ver maquina</h4>

                <p class="card-description">
                    Información referente a la maquina consultada.
                </p>

                <div class="form-group">
                    <label>Código | Nombre</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->codigo." | ".$maquina->nombre !!}">
                </div>

                <div class="form-group">
                    <label>Ubicación</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->ubicacion !!}">
                </div>

                <div class="form-group">
                    <label>Estado</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->estado !!}">
                </div>

                <div class="form-group">
                    <label>Fecha de instalación</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->f_instalacion !!}">
                </div>

                <div class="form-group">
                    <label>Fecha de ultimo mantenimiento</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->f_ultimo_mantenimiento !!}">
                </div>

                <div class="form-group">
                    <label>Fecha de proximo mantenimiento</label>
                    <input type="text" class="form-control form-control-lg" readonly value="{!! $maquina->f_proximo_mantenimiento !!}">
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-7 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Listado de mantenimientos</h4>
                <div class="table-responsive">
                    <table id="tabla_mantenimientos" class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    Tecnico
                                </th>
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Observaciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <br>

                <a href="{!! route('maquinas') !!}" class="btn btn-primary btn-lg btn-block" title="Volver al listado de maquinas">
                    <i class="mdi mdi-keyboard-return"></i>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tabla_mantenimientos').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{!! route('maquinas_mantenimientos',$maquina->id) !!}",
            "language": {
                url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "columns": [
                { "data": "tecnico" },
                { "data": "f_mantenimiento" },
                { "data": "observaciones" }
            ]
        });
    });
</script>
@endsection
