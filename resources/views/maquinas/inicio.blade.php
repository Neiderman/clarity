@extends('layouts.app')

@section('titulo')
Maquinas
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
        <div class="d-flex align-items-end flex-wrap"></div>
        <div class="d-flex justify-content-between align-items-end flex-wrap">
            <button onclick="location.href='{!! route('inicio') !!}';" class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0">
                <i class="mdi mdi-home text-muted"></i>
            </button>
            <button onclick="location.href='{!! route('maquinas_crear') !!}';"  class="btn btn-light bg-white btn-icon mr-3 mt-2 mt-xl-0" title="Crear nueva maquina">
                <i class="mdi mdi-plus text-muted"></i>
            </button>
        </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">Listado de maquinas</h4>
            <div class="table-responsive">
                <table id="tabla_maquinas" class="table table-striped">
                    <thead>
                        <tr>
                            <th>
                                Maquina
                            </th>
                            <th>
                                Ubicación
                            </th>
                            <th>
                                Ultimo mantenimiento
                            </th>
                            <th>
                                Proximo mantenimiento
                            </th>
                            <th>
                                Estado
                            </th>
                            <th>
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
  @endsection

@section('scripts')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        var table = $('#tabla_maquinas').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{!! route('maquinas_tabla') !!}",
            "language": {
                url: "//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json"
            },
            "columns": [
                { "data": "maquina" },
                { "data": "ubicacion" },
                { "data": "f_ultimo_mantenimiento" },
                { "data": "f_proximo_mantenimiento" },
                { "data": "estado" },
                { "data": "acciones" }
            ]
        });

        $('body').delegate('.btn_eliminar_maquina_nop','click', function(){
            Swal.fire({
                title: 'Error!',
                text: 'No puedes eliminar esta maquina porque cuenta con historico de mantenimientos.',
                icon: 'error',
                confirmButtonText: 'Cool'
            });

            return false;
        });

        $('body').delegate('.btn_eliminar_maquina','click', function(){
            var self = $(this);
            var maquina_id = self.attr('data-id');

            Swal.fire({

                icon: 'warning',
                title: 'Eliminar maquina',
                text: '¿Seguro que quieres eliminar esta maquina?',
                showCancelButton: true,
                confirmButtonText: 'Eliminar',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading()

            }).then((result) => {
                if (result.isConfirmed) {

                    if (maquina_id == 'empty') {
                        Swal.fire({
                            title: "Error",
                            icon: 'error',
                            text: 'Ocurrio un error eliminando la Maquina seleccionada.',
                        });
                    } else {
                        $.ajax({
                            type: "get",
                            url: "{!! route('maquinas_eliminar') !!}",
                            data: {
                                maquina_id: maquina_id
                            },
                            dataType: "text",
                            success: function (response) {
                                Swal.fire({
                                    title: "Todo salio bien!",
                                    icon: 'success',
                                    text: 'Maquina eliminada correctamente',
                                });
                                table.draw();
                            }, error: function (response) {
                                Swal.fire({
                                    title: "Error",
                                    icon: 'error',
                                    text: 'Ocurrio un error eliminando la Maquina seleccionada.',
                                });
                            }
                        });
                    }

                }
            })
        });
    });
</script>
@endsection
