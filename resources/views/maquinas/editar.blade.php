@extends('layouts.app')

@section('titulo')
Maquinas
@endsection

@section('contenido')
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body">
        <h4 class="card-title">Editar maquina</h4>
        <form method="POST" action="{{ route('maquinas_update',$maquina->id) }}">
            @csrf
            <div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Código</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="codigo" name="codigo" value="{!! old('codigo') ?: $maquina->codigo !!}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nombre</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nombre" name="nombre" value="{!! old('nombre') ?: $maquina->nombre !!}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Ubicación</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="ubicacion" id="ubicacion" required>{!! old('ubicacion') ?: $maquina->ubicacion !!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Fecha de instalación </label>
                            <div class="col-sm-9">
                                <input class="form-control" type="date" placeholder="dd/mm/yyyy" id="f_instalacion" name="f_instalacion" value="{!! old('f_instalacion') ?: $maquina->f_instalacion !!}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Estado {!!$maquina->estado!!}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="estado" name="estado" required>
                                <option value="1" {!! (old('estado') ?: $maquina->estado) == 1 ? "selected" : ""!!}>Activo</option>
                                <option value="2" {!! (old('estado') ?: $maquina->estado) == 2 ? "selected" : ""!!}>Inactivo</option>
                                <option value="3" {!! (old('estado') ?: $maquina->estado) == 3 ? "selected" : ""!!}>Averiado</option>
                            </select>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="button-group">
                    <a href="{!! route('maquinas') !!}" class="btn btn-primary btn-lg" title="Volver al listado de maquinas">
                        <i class="mdi mdi-keyboard-return"></i>
                    </a>
                    <button type="submit" class="btn btn-info btn-lg">
                        <i class="mdi mdi-content-save"></i>
                        Actualizar
                    </button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
