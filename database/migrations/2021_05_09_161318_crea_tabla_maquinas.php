<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreaTablaMaquinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquinas', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',20);
            $table->char('nombre',100);
            $table->text('ubicacion')->nullable();
            $table->dateTime('f_instalacion')->nullable();
            $table->dateTime('f_ultimo_mantenimiento')->nullable();
            $table->dateTime('f_proximo_mantenimiento')->nullable();

            /* Para el estado se definen 3 estados:
            * 1. Activa
            * 2. Inactiva
            * 3. Averiado
            */
            $table->smallInteger('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maquinas');
    }
}
