<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreaTablaHistoricoMantenimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_mantenimientos', function (Blueprint $table) {
            $table->id();
            $table->char('tecnico_tarjeta',25);
            $table->char('tecnico_nombre',100);
            $table->text('observaciones')->nullable();
            $table->dateTime('f_mantenimiento')->nullable();

            $table->unsignedBigInteger('maquina_id');
            $table->foreign('maquina_id')->references('id')->on('maquinas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_mantenimientos');
    }
}
